package org.acme.graph.model;

import java.util.ArrayList;
import java.util.List;

public class Path {
    private List<Edge> edges;

    public Path() {
        this.edges = new ArrayList<>();
    }

    public Path(List<Edge> edges) {
        this.edges = edges;
    }

    public List<Edge> getEdges() {
        return this.edges;
    }

    public double getLength(){
        Double longueur = 0.0;
        for (Edge e : this.edges) {
            longueur = longueur + e.getCost();
        }
        return longueur;
    }


}
